/*************************************
Jennifer Cagno
CPSC 1021, 002, F20
jcagno@g.clemson.edu
Instructor: Cathy Kettelstad
TAs: Nushrat Humaira and Evan Hastings
*************************************/
//this file includes

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;


typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0))); //using rand seed


  /*Create an array of 10 employees and fill information from standard input with prompt messages*/
	employee employeeArr[10]; //creating array
	int num = 1;
	for (int i = 0; i < 10; i++) { //loop to get user to enter all info
		cout << endl << "Enter employee " << num << " last name: ";
		cin >> employeeArr[i].lastName;
		cout << endl << "Enter employee " << num << " first name: ";
		cin >> employeeArr[i].firstName;
		cout << endl << "Enter employee " << num << " birth year: ";
		cin >> employeeArr[i].birthYear;
		cout << endl << "Enter employee " << num << " hourly wage: ";
		cin >> employeeArr[i].hourlyWage;
		num = num + 1; //iterating update
	}

  /*After the array is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
	 employee* employeeBegPtr = employeeArr; //pointer to begining
	 employee* employeeEndPtr = employeeArr+10; //pointer to one past end
	 //how do you create a pointer to a function?
	 random_shuffle(employeeBegPtr, employeeEndPtr, myrandom);

   /*Build a smaller array of 5 employees from the first five cards of the array created
    *above*/
		employee fiveEmployeeArr[5] = {employeeArr[0], employeeArr[1], employeeArr[2], employeeArr[3], employeeArr[4]};

    /*Sort the new array (by last name). Links to how to call this function is in the specs
     *provided*/
		 sort(employeeBegPtr, employeeEndPtr, name_order);


    /*Now print the array below */
		for (employee x: fiveEmployeeArr) { //loop to print all info

			cout << x.lastName << ", " << x.firstName << endl;
			cout << setw(10) << right << x.birthYear << endl;
			cout << setw(10) << right << showpoint << setprecision(2) << fixed << x.hourlyWage << endl;
		}
		cout <<endl;


  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs) {
  // IMPLEMENT
	bool returnVal = true;

	if (lhs.lastName < rhs.lastName) { //if left hand side is less than right hand side
		returnVal = true;
	}
	else { //if left hand side is greater than right hand side
		returnVal = false;
	}
	return (returnVal);
}
